
channel = {
    "id": -1000000000000,  # Put here your channel id
    "username": "",        # Put here your channel username if it is public
    "public": False        # Change this to True for public channels
}

group = {
    "id": -1000000000000,  # Put here your group id
    "username": "",        # Put here your group username if it is public
    "public": False        # Change this to True for public groups
}

bot = {
    "token": ""            # Put here your bot token
}
