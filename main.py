"""
Developed by OnionMaster03
Contacts: onionmaster03@protonmail.com
Licence: http://www.gnu.org/licenses/gpl-3.0.html
"""
from telegram.ext import MessageHandler, Updater, Filters
import config

# Initializing vars
channel = config.channel["id"]
group = config.group["id"]
cpublic = config.channel["public"]
gpublic = config.group["public"]
if cpublic:
    channellink = "https://t.me/{}/".format(config.channel["username"])
else:
    channellink = "https://t.me/c/{}/".format(str(channel).replace("-100", ""))
if gpublic:
    grouplink = "https://t.me/{}/".format(config.group["username"])
else:
    grouplink = "https://t.me/c/{}/".format(str(group).replace("-100", ""))

# Actual bot

def main():

    def mirrormessage(update, context):
        bot = context.bot
        if update.channel_post:
            if update.channel_post.forward_from:
                msg_id = update.channel_post.message_id
                bot.forwardMessage(chat_id=group, from_chat_id=channel, message_id=msg_id)
            elif update.channel_post.video:
                msg_id = update.channel_post.message_id
                if str(update.channel_post.caption) == "None":
                    text = ""
                else:
                    text = update.channel_post.caption_markdown_v2
                update2 = bot.sendVideo(chat_id=group, video=update.channel_post.video["file_id"],
                                        caption=text + " [\>](" + channellink + str(msg_id) + ")",
                                        parse_mode="MarkdownV2",
                                        disable_web_page_preview=True)
                bot.editMessageCaption(chat_id=channel,
                                       message_id=msg_id,
                                       caption=text + " [\>](" + grouplink + str(update2["message_id"]) + ")",
                                       parse_mode="MarkdownV2",
                                       disable_web_page_preview=True)
            elif update.channel_post.document:
                msg_id = update.channel_post.message_id
                if str(update.channel_post.caption) == "None":
                    text = ""
                else:
                    text = update.channel_post.caption_markdown_v2
                update2 = bot.sendDocument(chat_id=group, document=update.channel_post.document["file_id"],
                                           caption=text + " [\>](" + channellink + str(msg_id) + ")",
                                           parse_mode="MarkdownV2",
                                           disable_web_page_preview=True)
                bot.editMessageCaption(chat_id=channel, message_id=msg_id,
                                       caption=text + " [\>](" + grouplink + str(update2["message_id"]) + ")",
                                       parse_mode="MarkdownV2",
                                       disable_web_page_preview=True)
            elif update.channel_post.photo:
                msg_id = update.channel_post.message_id
                if str(update.channel_post.caption) == "None":
                    text = ""
                else:
                    text = update.channel_post.caption_markdown_v2
                update2 = bot.sendPhoto(chat_id=group,
                                        photo=update.channel_post.photo[len(update.channel_post.photo) - 1]["file_id"],
                                        caption=text + " [\>](" + channellink + str(msg_id) + ")",
                                        parse_mode="MarkdownV2",
                                        disable_web_page_preview=True)
                bot.editMessageCaption(chat_id=channel,
                                       message_id=msg_id,
                                       caption=text + " [\>](" + grouplink + str(update2["message_id"]) + ")",
                                       parse_mode="MarkdownV2",
                                       disable_web_page_preview=True)
            else:
                msg_id = update.channel_post.message_id
                try:
                    text = update.channel_post.text_markdown_v2
                    update2 = bot.sendMessage(text=text + " [\>](" + channellink + str(msg_id) + ")",
                                              chat_id=group,
                                              parse_mode="MarkdownV2",
                                              disable_web_page_preview=True)
                    bot.editMessageText(chat_id=channel,
                                        message_id=msg_id,
                                        text=text + " [\>](" + grouplink + str(update2["message_id"]) + ")",
                                        parse_mode="MarkdownV2",
                                        disable_web_page_preview=True)
                except:
                    bot.sendMessage(text="[\_](" + channellink + str(msg_id) + ")",
                                    chat_id=group,
                                    parse_mode="MarkdownV2")
        elif update.edited_channel_post:
            msg_id = update.edited_channel_post.message_id
            for i in update.edited_channel_post.entities:
                try:
                    f = i["url"].replace(grouplink, "")
                    try:
                        int(f)
                        msg_id2 = int(f)
                    except ValueError:
                        pass
                except AttributeError:
                    pass
            try:
                text = update.edited_channel_post.text_markdown_v2
                newtext = text.replace(" [\>](" + grouplink + str(msg_id2) + ")", " [\>](" + channellink + str(msg_id) + ")")
                print(bot.editMessageText(chat_id=group, message_id=msg_id2, text=newtext, parse_mode="MarkdownV2",
                                        disable_web_page_preview=True))
            except AttributeError:
                raise
                pass
            except ValueError:
                raise
                pass


    updater = Updater(token=config.bot["token"],
                      use_context=True)
    updater.dispatcher.add_handler(MessageHandler(Filters.chat(channel), mirrormessage))
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()